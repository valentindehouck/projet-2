# Projet 2

RESPONSIVE DESKTOP -> TABLETTE -> MOBILE

-- HTML --

Menu :
    Logo à gauche
    Navigation Hébergement renvoie vers la section
    Navigation Activité renvoie vers la section

Recherche : Chercher hébergement ville de leur choix
    Champ de saisie dans un formulaire (pour W3C)
    NON FONCTIONNEL

Filtres : En fonction de caractéristiques
    Changement d'apparence survol
    NON FONCTIONNEL

Carte d'hébergement : 
    Cliquable en intégralité, pas juste le titre

-- CSS --

UTILISER FLEXBOX ET PAS GRID

Breakpoint : MEDIA QUERIES
    992px pour ordi
    768px pour tablette 
    <768px Téléphone

Couleurs charte graphique :
    Bleu (#0065FC)
    Bleu plus clair (#DEEBFF)
    Gris (background) (#F2F2F2)

Police :
    Raleway (Google Font) https://fonts.google.com/specimen/Raleway

Icônes :
    CDN (BootstrapCDN) pour Font Awesome
    Temps de chargement facilité

-- OBLIGATIONS --

Aucun Framework
Valide avec W3C